module.exports = {
    "env" : {
        "es2021" : true,
        "node"   : true
    },
    "extends" : [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended"
    ],
    "parser" : "@typescript-eslint/parser",
    "parserOptions" : {
        "ecmaVersion" : 12,
        "sourceType"  : "module"
    },
    "plugins" : [
        "@typescript-eslint"
    ],
    "rules" : {
        // Possible Errors
        "no-await-in-loop"              : "error",
        "no-console"                    : "error",
        "no-extra-parens"               : ["error", "all"],
        "no-template-curly-in-string"   : "error",
        "require-atomic-updates"        : "error",

        // Best Practices
        "block-scoped-var"              : "error",
        "class-methods-use-this"        : "error",
        "consistent-return"             : "error",
        "default-case"                  : "error",
        "dot-location"                  : ["error", "object"],
        "eqeqeq"                        : "error",
        "guard-for-in"                  : "error",
        "no-alert"                      : "error",
        "no-caller"                     : "error",
        "no-else-return"                : "error",
        "no-eq-null"                    : "error",
        "no-eval"                       : "error",
        "no-extend-native"              : "error",
        "no-extra-bind"                 : "error",
        "no-extra-label"                : "error",
        "no-floating-decimal"           : "error",
        "no-implicit-coercion"          : "error",
        "no-iterator"                   : "error",
        "no-lone-blocks"                : "error",
        "no-new-func"                   : "error",
        "no-param-reassign"             : "error",
        "no-proto"                      : "error",
        "no-return-assign"              : "error",
        "no-return-await"               : "error",
        "no-script-url"                 : "error",
        "no-self-compare"               : "error",
        "no-throw-literal"              : "error",
        "no-unmodified-loop-condition"  : "error",
        "no-unused-expressions"         : "error",
        "no-useless-call"               : "error",
        "no-useless-concat"             : "error",
        "no-useless-return"             : "error",
        "no-void"                       : "error",
        "vars-on-top"                   : "error",
        "yoda"                          : "error",

        // Strict Mode

        // Variables
        "no-undef-init"                 : "error",
        "no-undefined"                  : "off",
        "no-use-before-define"          : ["warn", {functions : false}],

        // Stylistic Issues
        "brace-style"                   : ["error", "1tbs"],
        "camelcase"                     : "error",
        "comma-dangle"                  : ["error", "never"],
        "comma-spacing"                 : "error",
        "comma-style"                   : ["error", "last"],
        "eol-last"                      : ["error", "always"],
        "func-call-spacing"             : ["error", "never"],
        "indent"                        : ["error", 4],
        "linebreak-style"               : ["error", "unix"],
        "multiline-comment-style"       : ["error", "starred-block"],
        "no-lonely-if"                  : "error",
        "no-tabs"                       : "error",
        "no-trailing-spaces"            : "error",
        "no-unneeded-ternary"           : "error",
        "quote-props"                   : ["error", "consistent-as-needed"],
        "quotes"                        : ["error", "double"],
        "semi"                          : ["error", "never"],
        "semi-spacing"                  : ["error", {before : false, after : true}],
        "semi-style"                    : ["error", "last"],
        "space-before-blocks"           : "error",
        "space-before-function-paren"   : ["error", {
            anonymous  : "always",
            named      : "never",
            asyncArrow : "always"
        }],
        "space-in-parens"               : "error",
        "space-infix-ops"               : "error",
        "space-unary-ops"               : ["error", {words : true, nonwords : false}],
        "spaced-comment"                : ["error", "always"],
        "switch-colon-spacing"          : ["error", {after : true, before : true}],
        "unicode-bom"                   : "error",

        // ECMAScript 6
        "arrow-parens"                  : "error",
        "no-duplicate-imports"          : "error",
        "no-var"                        : "error",
        "prefer-const"                  : "error",
        "sort-imports"                  : ["error", {
            ignoreCase            : false,
            ignoreMemberSort      : false,
            memberSyntaxSortOrder : ["none", "all", "multiple", "single"]
        }],
        "symbol-description"            : "error"
    }
}
