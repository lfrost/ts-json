type Constraint<T = string> = (x:T) => boolean

// Constraint functions.
const isNegativeInteger : Constraint<number> = (x) => x < 0 && Number.isInteger(x)
const isPositiveInteger : Constraint<number> = (x) => x > 0 && Number.isInteger(x)
const isUrl             : Constraint         = (x) => URL.canParse(x)

export { Constraint, isNegativeInteger, isPositiveInteger, isUrl }
