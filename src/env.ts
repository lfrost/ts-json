import { JsonObject } from "./json"

/**
 */
class EnvError extends Error {
    readonly name = "EnvError"

    /**
     * @param message optional message
     */
    constructor(readonly message: string) {
        super(message)
    }
}

type EnvCast<T> = (name:string, s:string) => T

const envCastNumber: EnvCast<number> = (name:string, s:string) => {
    const n = Number(s)
    if (isNaN(n)) {
        throw new EnvError(`Invalid numeric environment variable ${name}.`)
    }
    return n
}

const falses = ["0", "f", "false", "n", "no", "off"]
const trues  = ["1", "t", "true", "y", "yes", "on"]

const envCastBoolean: EnvCast<boolean> = (name:string, s:string) => {
    if (falses.includes(s.toLowerCase())) {
        return false
    }
    if (trues.includes(s.toLowerCase())) {
        return true
    }
    throw new EnvError(`Invalid boolean environment variable ${name}.`)
}

/**
 * Get environment variable value.
 *
 * @param name environment variable name
 * @param cast function to cast data type
 * @return     value of the environment variable
 */
function getEnv<T = string>(name:string, cast?:EnvCast<T>) : T {
    const s = process.env[name]
    if (s === undefined) {
        throw new EnvError(`Unset environment variable ${name}.`)
    }
    if (typeof s !== "string") {
        throw new EnvError(`Non-string environment variable ${name}.`)
    }
    if (cast === undefined) {
        return s as unknown as T
    }
    return cast(name, s)
}

/**
 * Get optional environment variable value.
 *
 * @param name environment variable name
 * @param cast function to cast data type
 * @return     optional value of the environment variable
 */
function getEnvOpt<T = string>(name:string, cast?:EnvCast<T>) : T | undefined {
    if (process.env[name] === undefined) {
        return undefined
    }
    return getEnv(name, cast)
}

type getEnvAsJson = () => JsonObject

export { EnvError, EnvCast, envCastBoolean, envCastNumber, getEnv, getEnvAsJson, getEnvOpt }
