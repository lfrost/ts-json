import { JsonError, JsonObject, JsonType, JsonValidationRules } from "./json"
import { Constraint }                                           from "./constraints"

const isObject = (value:unknown) : boolean => typeof value === "object" && value !== null

/**
 * Validate a JSON object.
 */
class JsonValidator {
    private readonly error = (name:string, type:string) : JsonError => {
        const msg = (this.tag ? `[${this.tag}] ` : "") + `Invalid ${type} value for ${name}.`
        return new JsonError(msg)
    }

    /**
     * @param jsonObject JSON object for validation
     * @param tag        optional tag for exceptions
     */
    constructor(private readonly jsonObject:Readonly<JsonObject>, private readonly tag?:string) {}

    /**
     * Validate a primitive.
     *
     * @typeParam T data type to validate
     *
     * @param name       name of the attribute to validate
     * @param type       name of data type to validate (must match type parameter T)
     * @param constraint optional constraint on the value
     * @return           validated attribute as type T
     */
    validate<T = string>(this:JsonValidator, name:string, type:JsonType, constraint?:Constraint<T>) : T {
        const value : unknown = this.jsonObject[name]
        if (typeof value === type) {
            const v = value as T
            if (constraint === undefined || constraint(v)) {
                return v
            }
        }
        throw this.error(name, type)
    }

    /**
     * Validate an optional primitive.
     *
     * @typeParam T data type to validate
     *
     * @param name       name of the attribute to validate
     * @param type       name of data type to validate (must match type parameter T)
     * @param constraint optional constraint on the value
     * @return           validated optional attribute as type T
     */
    validateOpt<T = string>(
        this        : JsonValidator,
        name        : string,
        type        : JsonType,
        constraint? : Constraint<T>
    ) : T | undefined {
        if (this.jsonObject[name] === undefined || this.jsonObject[name] === null) {
            return undefined
        }
        return this.validate<T>(name, type, constraint)
    }

    /**
     * Validate an object.
     *
     * @param name name of the attribute to validate
     * @param jvr  JSON validation rules
     * @return     validated attribute as type T
     */
    validateObject<T>(this:JsonValidator, name:string, jvr:JsonValidationRules<T>) : Readonly<T> {
        const value : unknown = this.jsonObject[name]
        if (isObject(value)) {
            const jsonObject = value as JsonObject
            const jv = new JsonValidator(jsonObject)
            return jvr(jv)
        }
        throw this.error(name, "object")
    }

    /**
     * Validate an optional object.
     *
     * @param name name of the attribute to validate
     * @param jvr  JSON validation rules
     * @return     validated optional attribute as type T
     */
    validateObjectOpt<T>(this:JsonValidator, name:string, jvr:JsonValidationRules<T>) : Readonly<T> | undefined {
        if (this.jsonObject[name] === undefined || this.jsonObject[name] === null) {
            return undefined
        }
        return this.validateObject<T>(name, jvr)
    }

    /**
     * Validate an array.
     *
     * @param name name of the attribute to validate
     * @return     validated attribute as an array of type T
     */
    validateArray<T = string>(this:JsonValidator, name:string, type:JsonType) : ReadonlyArray<T> {
        const value : unknown = this.jsonObject[name]
        if (Array.isArray(value)) {
            const a = value as Array<unknown>
            for (const s of a) {
                if (typeof s !== type) {
                    throw this.error(name, `array of ${type}`)
                }
            }
            return a as ReadonlyArray<T>
        }
        throw this.error(name, `array of ${type}`)
    }

    /**
     * Validate an optional array.
     *
     * @typeParam T data type to validate
     *
     * @param name name of the attribute to validate
     * @param type name of the array element data type to validate (must match type parameter T)
     * @return     validated optional attribute as an array of type T
     */
    validateArrayOpt<T = string>(this:JsonValidator, name:string, type:JsonType) : ReadonlyArray<T> | undefined {
        if (this.jsonObject[name] === undefined || this.jsonObject[name] === null) {
            return undefined
        }
        return this.validateArray<T>(name, type)
    }

    /**
     * Validate an array of objects.
     *
     * @param name name of the attribute to validate
     * @param jvr  JSON validation rules
     * @return     validated attribute as type T
     */
    validateArrayOfObjects<T>(this:JsonValidator, name:string, jvr:JsonValidationRules<T>) : ReadonlyArray<T> {
        const value : unknown = this.jsonObject[name]
        if (Array.isArray(value)) {
            const a = value as Array<unknown>
            for (const s of a) {
                if (!isObject(s)) {
                    throw this.error(name, "array of object")
                }
                const jsonObject = s as JsonObject
                const jv = new JsonValidator(jsonObject)
                jvr(jv)
            }
            return a as ReadonlyArray<T>
        }
        throw this.error(name, "array of object")
    }

    /**
     * Validate an optional array of objects.
     *
     * @typeParam T data type to validate
     *
     * @param name name of the attribute to validate
     * @param jvr  JSON validation rules
     * @return     validated optional attribute as type T
     */
    validateArrayOfObjectsOpt<T>(this:JsonValidator, name:string, jvr:JsonValidationRules<T>) : ReadonlyArray<T> | undefined {
        if (this.jsonObject[name] === undefined || this.jsonObject[name] === null) {
            return undefined
        }
        return this.validateArrayOfObjects<T>(name, jvr)
    }
}

export { JsonValidator }
