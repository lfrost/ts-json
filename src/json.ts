import { JsonValidator } from "./JsonValidator"

/**
 */
class JsonError extends Error {
    readonly name = "JsonError"

    /**
     * @param message optional message
     */
    constructor(readonly message: string) {
        super(message)
    }
}

type JsonObject = Record<string, unknown>

enum JsonType {
    boolean = "boolean",
    number  = "number",
    string  = "string",
}

type JsonValidationRules<T> = (jv:JsonValidator) => T

const isObject = (value:unknown) : boolean => typeof value === "object" && value !== null

export { JsonError, JsonObject, JsonType, JsonValidationRules, isObject }
