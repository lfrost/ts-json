import { isNegativeInteger, isPositiveInteger, isUrl } from "../constraints"

describe("env unit tests", () => {
    const ORIGINAL_ENV = process.env

    beforeEach(() => {
        process.env = { ...ORIGINAL_ENV }
    })

    test("isNegativeInteger true on negative integer", () => {
        expect(isNegativeInteger(-1)).toStrictEqual(true)
    })

    test("isNegativeInteger false on zero", () => {
        expect(isNegativeInteger(0)).toStrictEqual(false)
    })

    test("isNegativeInteger false on positive integer", () => {
        expect(isNegativeInteger(1)).toStrictEqual(false)
    })

    test("isNegativeInteger false on non-integer", () => {
        expect(isNegativeInteger(-1.1)).toStrictEqual(false)
    })

    test("isPositiveInteger true on positive integer", () => {
        expect(isPositiveInteger(1)).toStrictEqual(true)
    })

    test("isPositiveInteger false on zero", () => {
        expect(isPositiveInteger(0)).toStrictEqual(false)
    })

    test("isPositiveInteger false on positive integer", () => {
        expect(isPositiveInteger(-1)).toStrictEqual(false)
    })

    test("isPositiveInteger false on non-integer", () => {
        expect(isPositiveInteger(1.1)).toStrictEqual(false)
    })

    test("isUrl true on valid URL", () => {
        expect(isUrl("https://www.cnz.com")).toStrictEqual(true)
    })

    test("isUrl false on invalid URL", () => {
        expect(isUrl("none")).toStrictEqual(false)
    })
})
