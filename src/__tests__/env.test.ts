import { EnvError, envCastBoolean, envCastNumber, getEnv, getEnvOpt } from "../env"

describe("env unit tests", () => {
    const ORIGINAL_ENV = process.env

    beforeEach(() => {
        process.env = { ...ORIGINAL_ENV }
    })

    test("getEnv returns set var", () => {
        process.env.SHELL = "/bin/bash"
        const result: string = getEnv("SHELL")
        expect(result).toStrictEqual("/bin/bash")
    })

    test("getEnv throws on unset var", () => {
        delete process.env.SHELL
        expect(() => getEnv("SHELL")).toThrow(new EnvError("Unset environment variable SHELL."))
    })

    test("getEnv throws on non-string var", () => {
        process.env.COUNT = 123 as unknown as string
        expect(() => getEnv("COUNT")).toThrow(new EnvError("Non-string environment variable COUNT."))
    })

    test("getEnv casts as boolean false", () => {
        process.env.FLAG = "False"
        expect(getEnv<boolean>("FLAG", envCastBoolean)).toStrictEqual(false)
    })

    test("getEnv casts as boolean true", () => {
        process.env.FLAG = "TRUE"
        expect(getEnv<boolean>("FLAG", envCastBoolean)).toStrictEqual(true)
    })

    test("getEnv throws on invalid boolean", () => {
        process.env.FLAG = "FALS"
        expect(() => getEnv<boolean>("FLAG", envCastBoolean)).toThrow(new EnvError("Invalid boolean environment variable FLAG."))
    })

    test("getEnv casts as number", () => {
        process.env.COUNT = "123"
        expect(getEnv<number>("COUNT", envCastNumber)).toStrictEqual(123)
    })

    test("getEnv throws on invalid number", () => {
        process.env.COUNT = "123A"
        expect(() => getEnv<number>("COUNT", envCastNumber)).toThrow(new EnvError("Invalid numeric environment variable COUNT."))
    })

    test("getEnvOpt returns set var", () => {
        process.env.SHELL = "/bin/bash"
        const result = getEnvOpt("SHELL")
        expect(result).toStrictEqual("/bin/bash")
    })

    test("getEnvOpt returns unset var", () => {
        delete process.env.SHELL
        const result = getEnvOpt("SHELL")
        expect(result).toStrictEqual(undefined)
    })
})
