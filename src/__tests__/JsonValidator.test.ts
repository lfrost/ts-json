import { isNegativeInteger, isPositiveInteger } from "../constraints"
import { JsonType as JT }                       from "../json"
import { JsonValidator }                        from "../JsonValidator"

interface Address {
    city      : string,
    stateCode : string
}

const person = {
    id   : 6,
    name : "Patrick McGoohan",
    free : true,
    address : {
        city      : "The Village",
        stateCode : "AA"
    },
    others : [2, 3, 4, 5],
    addresses : [
        {
            city      : "Cincinnati",
            stateCode : "OH"
        },
        {
            city      : "Chicago",
            stateCode : "IL"
        }
    ]
}

describe("JsonValidator unit tests", () => {
    const jv = new JsonValidator(person)
    const jvt = new JsonValidator(person, "some-tag")
    const addressValidator = (jv:JsonValidator) => {
        return {
            city      : jv.validate("city", JT.string),
            stateCode : jv.validate("stateCode", JT.string)
        }
    }
    const expectedAddress = {
        city      : "The Village",
        stateCode : "AA"
    }

    describe("boolean", () => {
        test("boolean validates correctly", () => {
            const result = jv.validate<boolean>("free", JT.boolean)
            expect(result).toStrictEqual(true)
        })

        test("defined optional boolean validates correctly", () => {
            const result = jv.validateOpt<boolean>("free", JT.boolean)
            expect(result).toStrictEqual(true)
        })

        test("undefined optional boolean validates correctly", () => {
            const result = jv.validateOpt<boolean>("none", JT.boolean)
            expect(result).toStrictEqual(undefined)
        })

        test("invalid boolean throws", () => {
            expect(() => jv.validate<boolean>("name", JT.boolean)).toThrow(new Error("Invalid boolean value for name."))
        })

        test("invalid boolean throws with tag", () => {
            expect(() => jvt.validate<boolean>("name", JT.boolean)).toThrow(new Error("[some-tag] Invalid boolean value for name."))
        })
    })

    describe("number", () => {
        test("number validates correctly", () => {
            const result = jv.validate<number>("id", JT.number)
            expect(result).toStrictEqual(6)
        })

        test("defined optional number validates correctly", () => {
            const result = jv.validateOpt<number>("id", JT.number)
            expect(result).toStrictEqual(6)
        })

        test("undefined optional number validates correctly", () => {
            const result = jv.validateOpt<number>("none", JT.number)
            expect(result).toStrictEqual(undefined)
        })

        test("number that passes constraint validates correctly", () => {
            const result = jv.validateOpt<number>("id", JT.number, isPositiveInteger)
            expect(result).toStrictEqual(6)
        })

        test("number that fails constraint throws", () => {
            expect(() => jv.validateOpt<number>("id", JT.number, isNegativeInteger)).toThrow(new Error("Invalid number value for id."))
        })
    })

    describe("string", () => {
        test("string validates correctly", () => {
            const result = jv.validate("name", JT.string)
            expect(result).toStrictEqual("Patrick McGoohan")
        })

        test("defined optional string validates correctly", () => {
            const result = jv.validateOpt("name", JT.string)
            expect(result).toStrictEqual("Patrick McGoohan")
        })

        test("undefined optional string validates correctly", () => {
            const result = jv.validateOpt("none", JT.string)
            expect(result).toStrictEqual(undefined)
        })
    })

    describe("object", () => {
        test("object validates correctly", () => {
            const result = jv.validateObject<Address>("address", addressValidator)
            expect(result).toStrictEqual(expectedAddress)
        })

        test("defined optional object validates correctly", () => {
            const result = jv.validateObjectOpt<Address>("address", addressValidator)
            expect(result).toStrictEqual(expectedAddress)
        })

        test("undefined optional object validates correctly", () => {
            const result = jv.validateObjectOpt<Address>("none", addressValidator)
            expect(result).toStrictEqual(undefined)
        })

        test("invalid object throws", () => {
            expect(() => jv.validateObjectOpt<Address>("name", addressValidator)).toThrow(new Error("Invalid object value for name."))
        })
    })

    describe("array", () => {
        test("array validates correctly", () => {
            const result = jv.validateArray<number>("others", JT.number)
            expect(result).toStrictEqual([2, 3, 4, 5])
        })

        test("defined optional array validates correctly", () => {
            const result = jv.validateArrayOpt<number>("others", JT.number)
            expect(result).toStrictEqual([2, 3, 4, 5])
        })

        test("undefined optional array validates correctly", () => {
            const result = jv.validateArrayOpt<number>("none", JT.number)
            expect(result).toStrictEqual(undefined)
        })

        test("invalid array that is not an array throws", () => {
            expect(() => jv.validateArrayOpt<number>("name", JT.number)).toThrow(new Error("Invalid array of number value for name."))
        })

        test("invalid array with invalid element type throws", () => {
            expect(() => jv.validateArrayOpt("others", JT.string)).toThrow(new Error("Invalid array of string value for others."))
        })
    })

    describe("array of objects", () => {
        test("array validates correctly", () => {
            const result:ReadonlyArray<Address> = jv.validateArrayOfObjects<Address>("addresses", addressValidator)
            expect(result).toStrictEqual(person.addresses)
        })

        test("defined optional array validates correctly", () => {
            const result:ReadonlyArray<Address> | undefined = jv.validateArrayOfObjectsOpt<Address>("addresses", addressValidator)
            expect(result).toStrictEqual(person.addresses)
        })

        test("undefined optional array validates correctly", () => {
            const result = jv.validateArrayOfObjectsOpt<Address>("none", addressValidator)
            expect(result).toStrictEqual(undefined)
        })

        test("invalid array that is not an array throws", () => {
            expect(() => jv.validateArrayOfObjectsOpt<Address>("name", addressValidator)).toThrow(new Error("Invalid array of object value for name."))
        })

        test("invalid array with invalid element type throws", () => {
            expect(() => jv.validateArrayOfObjectsOpt<Address>("others", addressValidator)).toThrow(new Error("Invalid array of object value for others."))
        })
    })

    describe("nulls", () => {
        const nullPerson = {
            id   : null,
            name : null,
            free : null,
            address : null,
            others : null,
            addresses : null
        }
        const nullJv = new JsonValidator(nullPerson)

        test("null boolean throws", () => {
            expect(() => nullJv.validate<boolean>("free", JT.boolean)).toThrow(new Error("Invalid boolean value for free."))
        })

        test("null optional boolean validates correctly", () => {
            const result = nullJv.validateOpt<boolean>("free", JT.boolean)
            expect(result).toStrictEqual(undefined)
        })

        test("null number throws", () => {
            expect(() => nullJv.validate<number>("id", JT.number)).toThrow(new Error("Invalid number value for id."))
        })

        test("null optional number validates correctly", () => {
            const result = nullJv.validateOpt<number>("id", JT.number)
            expect(result).toStrictEqual(undefined)
        })

        test("null string throws", () => {
            expect(() => nullJv.validate("name", JT.string)).toThrow(new Error("Invalid string value for name."))
        })

        test("null optional string validates correctly", () => {
            const result = nullJv.validateOpt("name", JT.string)
            expect(result).toStrictEqual(undefined)
        })

        test("null object throws", () => {
            expect(() => nullJv.validateObject<Address>("address", addressValidator)).toThrow(new Error("Invalid object value for address."))
        })

        test("null optional object validates correctly", () => {
            const result = nullJv.validateObjectOpt<Address>("address", addressValidator)
            expect(result).toStrictEqual(undefined)
        })

        test("null array throws", () => {
            expect(() => nullJv.validateArray<number>("others", JT.number)).toThrow(new Error("Invalid array of number value for others."))
        })

        test("null optional array validates correctly", () => {
            const result = nullJv.validateArrayOpt<number>("others", JT.number)
            expect(result).toStrictEqual(undefined)
        })



        test("null array of objects throws", () => {
            expect(() => nullJv.validateArrayOfObjects<Address>("addresses", addressValidator)).toThrow(new Error("Invalid array of object value for addresses."))
        })

        test("null optional array of objects validates correctly", () => {
            const result = nullJv.validateArrayOfObjectsOpt<Address>("addresses", addressValidator)
            expect(result).toStrictEqual(undefined)
        })
    })
})
