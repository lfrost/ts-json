import { isObject } from "../json"

describe("json unit tests", () => {
    test("isObject false for null", () => {
        expect(isObject(null)).toStrictEqual(false)
    })

    test("isObject false for string", () => {
        expect(isObject("abc")).toStrictEqual(false)
    })

    test("isObject true for object", () => {
        expect(isObject({ name:"Patrick" })).toStrictEqual(true)
    })
})
